#include "ipre.hpp"

#include <benchmark/benchmark.h>

namespace PPANN {
class IPREBenchmark : public ::benchmark::Fixture {
   protected:
    IPREBenchmark() {
        // Init core and setup.
        core_init();
        pc_param_set_any();
    }
};

BENCHMARK_DEFINE_F(IPREBenchmark, Setup)
(benchmark::State& state) {
    auto size = (int)state.range(0);
    for (auto _ : state) {
        benchmark::DoNotOptimize(setup(size));
    }
}

BENCHMARK_DEFINE_F(IPREBenchmark, Encrypt)
(benchmark::State& state) {
    auto size = (int)state.range(0);

    Key key = setup(size);
    int* x = new int[size];
    for (size_t i = 0; i < size; i++) {
        x[i] = i;
    }

    for (auto _ : state) {
        benchmark::DoNotOptimize(enc(key, x, size));
    }
}

BENCHMARK_DEFINE_F(IPREBenchmark, Eval)
(benchmark::State& state) {
    // TODO(weiqi): figure out how to properly benchmark given the `bound`
    // argument.

    int size = 10;
    int bound = 150;

    int x[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int y[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 100};
    // Initialize the scheme.
    Key key = setup(size);
    // Encrypt the messages.
    Ct ct_x = enc(key, x, size);
    Ct ct_y = enc(key, y, size);

    for (auto _ : state) {
        benchmark::DoNotOptimize(eval(key, ct_x, ct_y, size, bound));
    }
}

BENCHMARK_REGISTER_F(IPREBenchmark, Setup)
    ->RangeMultiplier(2)
    ->Range(1, 1 << 10)
    ->Complexity()
    ->Iterations(1 << 10)
    ->Unit(benchmark::kMicrosecond);

BENCHMARK_REGISTER_F(IPREBenchmark, Encrypt)
    ->RangeMultiplier(2)
    ->Range(1, 1 << 10)
    ->Complexity()
    ->MinTime(10)
    ->Unit(benchmark::kMillisecond);

BENCHMARK_REGISTER_F(IPREBenchmark, Eval)
    ->MinTime(10)
    ->Unit(benchmark::kMillisecond);

}  // namespace PPANN

BENCHMARK_MAIN();
