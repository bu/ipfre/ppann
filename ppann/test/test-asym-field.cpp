#include "asym-field.hpp"
#include "gtest/gtest.h"

namespace PPANN {

class AsymFieldTest : public ::testing::Test {
   protected:
    bn_t N_;

    AsymFieldTest() {
        // Init core and setup.
        core_init();
        pc_param_set_any();

        // Get order.
        pc_get_ord(N_);
    }
};

TEST_F(AsymFieldTest, ZpZero) {
    asymZp x = asym::zp_zero(N_);
    ASSERT_EQ(1, asym::zp_cmp_int(x, 0));
}

TEST_F(AsymFieldTest, ZpOne) {
    asymZp x = asym::zp_one(N_);
    ASSERT_EQ(1, asym::zp_cmp_int(x, 1));
}

TEST_F(AsymFieldTest, ZpCopy) {
    asymZp x = asym::zp_from_int(10, N_);
    asymZp y = asym::zp_copy(x);
    ASSERT_EQ(1, asym::zp_cmp(x, y));
}

TEST_F(AsymFieldTest, ZpFromInt) {
    asymZp x = asym::zp_from_int(3, N_);
    ASSERT_EQ(1, asym::zp_cmp_int(x, 3));
}

TEST_F(AsymFieldTest, ZpAdd) {
    asymZp x = asym::zp_from_int(10, N_);
    asymZp y = asym::zp_from_int(20, N_);
    asymZp z = asym::zp_add(x, y);
    ASSERT_EQ(1, asym::zp_cmp_int(z, 30));
}

TEST_F(AsymFieldTest, ZpNeg) {
    asymZp x = asym::rand_zp(N_);
    asymZp y = asym::zp_neg(x);
    asymZp z = asym::zp_add(x, y);
    ASSERT_EQ(1, asym::zp_cmp_int(z, 0));
}

TEST_F(AsymFieldTest, ZpMul) {
    asymZp x = asym::zp_from_int(10, N_);
    asymZp y = asym::zp_from_int(20, N_);
    asymZp z = asym::zp_mul(x, y);
    ASSERT_EQ(1, asym::zp_cmp_int(z, 200));
}

TEST_F(AsymFieldTest, ZpInv) {
    asymZp x = asym::rand_zp(N_);
    asymZp y = asym::zp_inv(x);
    asymZp z = asym::zp_mul(x, y);
    ASSERT_EQ(1, asym::zp_cmp_int(z, 1));
}

}  // namespace PPANN

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
