FROM dbogatov/docker-images:relic-latest-multi-arch

# Copy the files over to working directory.
RUN mkdir -p /home/project/build
COPY . /home/project

# Build the project.
WORKDIR "/home/project/build"
RUN cmake /home/project
RUN make

# Execute the test.
RUN ctest
